<!doctype html>
<html class="no-js" lang="de">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php wp_title(); ?></title>
        <link rel="stylesheet" href="http://www.liebeschweiz.com/assets/css/app.css" />
        <script src="http://www.liebeschweiz.com/assets/js/vendor/modernizr.js"></script>
        <?php
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'wp_generator');
        wp_head();
        ?>
    </head>
    <body>
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">
                <div class="row" id="blog_header">
                    <div class="columns small-12 large-12 medium-12">
                        <h1>Liebe Schweiz ...</h1>
                        <p>Ein Ausl&auml;nder &uuml;ber seine neue Heimat</p>
                    </div>
                </div>