<?php if (have_posts()) while (have_posts()) : the_post(); ?>
    <div class="row">
        <div class="columns large-12 medium-12 small-12">
            <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            <small>Von <?php the_author_posts_link(); ?>, am <?php echo the_time(get_option('date_format')); ?> <?php edit_post_link(_('Bearbeiten'), '(', ')'); ?></small>
        </div>
    </div>
    <div class="row">
        <div class="columns large-12 medium-12 small-12">
            <?php the_content(); ?>
        </div>
    </div>
    <?php if (is_single()) : ?>
        <div class="row">
            <div class="columns large-12 medium-12 small-12">
                <h2><a name="comments" href="<?php the_permalink(); ?>#comments" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Kommentare</a></h2>
                <div id="disqus_thread" style="background: #fff; display: block; position: relative;"></div>
                <script type="text/javascript">
                    /* * * CONFIGURATION VARIABLES * * */
                    var disqus_shortname = 'liebeschweizcom';
                    (function() {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
            </div>
        </div>
    <?php endif; ?>
<?php endwhile; ?>
