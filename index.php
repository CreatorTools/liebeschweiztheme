<?php get_header(); ?>
<div class="row" id="blog_main">
    <div class="columns small-12 medium-8 large-8" id="blog_content">
        <?php get_template_part('post'); ?>
    </div>
    <div class="columns small-12 medium-4 large-4" id="blog_sidebar">
        <?php get_sidebar(); ?>
    </div>
</div>
<div class="row" id="blog_footer">
    <div class="columns small-12 large-12 medium-12">
        &copy; David Schneider 2015
    </div>
</div>
<?php get_footer(); ?>